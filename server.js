import Koa from 'koa'

import logger from 'koa-logger' // a simple and beautiful terminal logger
import compress from 'koa-compress' // to enable gzip compression etc
// import error from 'koa-json-error' // replaced by own middleware: ./resonseHandler.js
import route from 'koa-route' // basic router
import legit from 'koa-legit' // some magic middleware to prevent xss and sql injection etc (not tested yet)
import ratelimit from 'koa-ratelimit' // limits the request per ip
import bodyParser from 'koa-bodyparser' // enables access to post body
import Redis from 'ioredis' // fancy redis client, used for ratelimit middleware

import a7s from './a7s' // own analytics (a7n) middleware. WARNING: very basic, simple and small
import responseHandler from './responseHandler' // response handling in a fancy way (stolen from https://github.com/posquit0/koa-rest-api-boilerplate/)

import exampleController from './controller/exampleController' // controller for profile-based actions

const app = new Koa() // initialize the koa app

app.use(logger()) // adds koa-logger middleware

// saves some user-data in mongodb
app.use(async (ctx, next) => {
    const start = new Date()
    await next()
    a7s.insert({
        timestamp: start.getTime(),
        host: ctx.ip,
        request: {
            url: ctx.request.href,
            method: ctx.method,  
            headers: ctx.headers 
        },
        response: {
            headers: ctx.res.getHeaders()
        },
        status: ctx.status,
    })
})

// adds things like ctx.res.ok(data) to
app.use(responseHandler())

// adds ratelimit middleware (max 200 requests / minute)
app.use(ratelimit({
    db: new Redis(),
    duration: 60000, // 1 min
    max: 200, // max requests per $duration
    errorMessage: {status: 429, message: 'Too many requests (limit: 200 requests / minute)'},
    id: ctx => ctx.ip,
    headers: {
        remaining: 'X-RateLimit-Remaning',
        reset: 'X-RateLimit-Reset',
        total: 'X-RateLimit-Total'
    },
    disableHeader: false
}))

app.use(bodyParser())

app.use(route.get('/', exampleController.get))
app.use(route.post('/', exampleController.add))

app.use(compress())
app.use(legit())

console.dir(route.stack)

// app.use(route.get('/:key/', (ctx, key, next) => {
//     ctx.res.ok('okii')
// }))



// app.use(async (ctx, next) => {
//     ctx.res.notAllowed()
// })



app.listen(8813)