export default {
    UNABLE_TO_FETCH: {
        code: '0x10',
        message: 'Unable to fetch data with this vin / id'
    },
    INVALID_API_KEY: {
        code: '0x01',
        message: 'Invalid api key'
    },
    VIN_ALREADY_EXISTS: {
        code: '0x20',
        message: 'Car with same VIN already exists in database'
    }
}