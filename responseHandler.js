const statusCodes = {
    CONTINUE: 100,
    OK: 200,
    CREATED: 201,
    ACCEPTED: 202,
    NO_CONTENT: 204,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    NOT_ALLOWED: 405,
    REQUEST_TIMEOUT: 408,
    UNPROCESSABLE_ENTITY: 422,
    INTERNAL_SERVER_ERROR: 500,
    NOT_IMPLEMENTED: 501,
    BAD_GATEWAY: 502,
    SERVICE_UNAVAILABLE: 503,
    GATEWAY_TIME_OUT: 504
}

module.exports = () => {

    return async(ctx, next) => {
        ctx.statusCodes = statusCodes

        ctx.res.success = (data, message = '') => {
            ctx.status = ctx.status < 400 ? ctx.status : statusCodes.OK
            ctx.body = {
                status: 'success',
                result: data,
                message: message ? message : undefined
                // theoretically here you could do something like:
                //  token: tokenHandler.generateNewToken
                // as well as proofing the current token
            }
        }

        ctx.res.fail = error => {
            ctx.status = ctx.status >= 400 && ctx.status < 500 ? ctx.status : statusCodes.BAD_REQUEST
            ctx.body = {
                status: 'fail',
                error
            }
        }
        ctx.res.error = error => {
            ctx.status = ctx.status < 500 ? statusCodes.INTERNAL_SERVER_ERROR : ctx.status
            ctx.body = {
                status: 'error',
                error
            }
        }

        // success
        ctx.res.ok = (data, message) => {
            ctx.status = statusCodes.OK
            ctx.res.success(data, message)
        }
        ctx.res.created = (data, message) => {
            ctx.status = statusCodes.CREATED
            ctx.res.success(data, message)
        }
        ctx.res.accepted = (data, message) => {
            ctx.status = statusCodes.ACCEPTED
            ctx.res.success(data, message)
        }
        ctx.res.noContent = (data, message) => {
            ctx.status = statusCodes.NO_CONTENT
            ctx.res.success(data, message)
        }

        // fail
        ctx.res.badRequest = error => {
            ctx.status = statusCodes.BAD_REQUEST
            ctx.res.fail(error)
        }
        ctx.res.forbidden = error => {
            ctx.status = statusCodes.FORBIDDEN
            ctx.res.fail(error)
        }
        ctx.res.notFound = error => {
            ctx.status = statusCodes.NOT_FOUND
            ctx.res.fail(error)
        }
        ctx.res.notAllowed = error => {
            ctx.status = statusCodes.NOT_ALLOWED
            ctx.res.fail(error)
        }
        ctx.res.requestTimeout = error => {
            ctx.status = statusCodes.REQUEST_TIMEOUT
            ctx.res.fail(error)
        }
        ctx.res.unprocessableEntity = error => {
            ctx.status = statusCodes.UNPROCESSABLE_ENTITY
            ctx.res.fail(error)
        }

        // error
        ctx.res.internalServerError = error => {
            ctx.status = statusCodes.INTERNAL_SERVER_ERROR
            ctx.res.error(error)
        }
        ctx.res.notImplemented = error => {
            ctx.status = statusCodes.NOT_IMPLEMENTED
            ctx.res.error(error)
        }
        ctx.res.badGateway = error => {
            ctx.status = statusCodes.BAD_GATEWAY
            ctx.res.error(error)
        }
        ctx.res.serviceUnavailable = error => {
            ctx.status = statusCodes.SERVICE_UNAVAILABLE
            ctx.res.error(error)
        }
        ctx.res.gatewayTimeOut = error => {
            ctx.status = statusCodes.GATEWAY_TIME_OUT
            ctx.res.error(error)
        }
        await next()
    }

}